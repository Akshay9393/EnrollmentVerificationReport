﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentVerificationReport
{
    class CreateReportBO
    {

        public Boolean createReport()
        {

            List<EnrollmentRecordPOCO> listEnroll = getCreateReportDAO().GetEnrollMentReport();

            this.writeEnrollReport(listEnroll);
            List<VerificationRecordsPOCO> listVerify = getCreateReportDAO().GetVerficationReport();
            this.writeVerificationReport(listVerify);
            return false;
        }

        #region
        public Boolean writeEnrollReport(List<EnrollmentRecordPOCO> listEnroll)
        {
            Dictionary<string, EnrollmentRecordPOCO> hset = new Dictionary<string, EnrollmentRecordPOCO>();
            StringBuilder sb = new StringBuilder("SessionId Ucid Timestamp	ActivityFinal	Status");
            foreach(EnrollmentRecordPOCO en in listEnroll)
            {
                string key = en.Session_Id.ToString();
                if (en.Ucid != null || en.Ucid != "")
                {
                    key = en.Session_Id.ToString() + en.Ucid;
                }
                //this Dictionary object will be filled from lower date to higher latest dates. The session id with latest dates will override the entries for old same session id.
                if (hset.ContainsKey(key))
                {
                    hset[key] = en;
                }
                else
                {
                    hset.Add(key, en);
                }
            }
            foreach(var item in hset)
            {
                EnrollmentRecordPOCO db = (EnrollmentRecordPOCO)item.Value;
                sb.Append(Environment.NewLine);
                sb.Append(db.ToString());            
            }
            AppLogger.MakeEntry(sb.ToString(), AppUtils.GetYesterdayDateTime("TransactionSummary"));
            return false;
        }
        #endregion


        #region
        public Boolean writeVerificationReport(List<VerificationRecordsPOCO> listVerify)
        {
            Dictionary<string, VerificationRecordsPOCO> hset = new Dictionary<string, VerificationRecordsPOCO>();
            StringBuilder sb = new StringBuilder();
            foreach (VerificationRecordsPOCO en in listVerify)
            {
                string key = en.Session_Id.ToString();
                if (en.Ucid!=null||en.Ucid!="")
                {
                   key = en.Session_Id.ToString() + en.Ucid;
                }
                //this Dictionary object will be filled from lower date to higher latest dates. The session id with latest dates will override the entries for old same session id.
                if (hset.ContainsKey(key))
                {
                    hset[key] = en;
                }
                else
                {
                    hset.Add(key,en);
                }
            }
            foreach (var item in hset)
            {
                VerificationRecordsPOCO db = (VerificationRecordsPOCO)item.Value;
                sb.Append(db.ToString());
                sb.Append(Environment.NewLine);
            }
            //Get the fileName for the report.

            AppLogger.MakeEntry(sb.ToString(), AppUtils.GetYesterdayDateTime("TransactionSummary"));
            return false;
        }
        #endregion
        public CreateReportDAO getCreateReportDAO()
        {
          return  new CreateReportDAO();
        }
    }
}
