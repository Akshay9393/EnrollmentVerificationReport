﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace EnrollmentVerificationReport
{
    class CreateReportDAO
    {
        static string enrollmentQuery = System.Configuration.ConfigurationSettings.AppSettings["EnrollmentQuery"];
        static string verificationQuery = System.Configuration.ConfigurationSettings.AppSettings["VerficationQuery"];
        static string customStartDate = System.Configuration.ConfigurationSettings.AppSettings["CustomStartDate"];
        static string customEndDate = System.Configuration.ConfigurationSettings.AppSettings["CustomEndDate"];
        
        #region  Get Enrollment records
        public List<EnrollmentRecordPOCO> GetEnrollMentReport()
        {
            OracleDataReader oracleDataReader = null;
            string connection = AppUtils.getConnectionString();
            if (customStartDate != "" && customEndDate != "")
            {
                enrollmentQuery = enrollmentQuery.Replace("?START_DATE?", "'"+customStartDate+"'");
                enrollmentQuery = enrollmentQuery.Replace("?END_DATE?", "'"+customEndDate+"'");
            }
            else
            {
                var currentDate = DateTime.Now;
                var yesterdayDate = DateTime.Today.AddDays(-1);
                enrollmentQuery = enrollmentQuery.Replace("?START_DATE?", "'" + AppUtils.GetDateZeroTime(currentDate).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
                enrollmentQuery = enrollmentQuery.Replace("?END_DATE?", "'" + AppUtils.GetDateZeroTime(yesterdayDate).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
            }
            
            Console.WriteLine(enrollmentQuery);
            AppLogger.MakeEntry(enrollmentQuery);
            OracleConnection conn = new OracleConnection(connection);
            List<EnrollmentRecordPOCO> enrollList = new List<EnrollmentRecordPOCO>();

            try
            {
                conn.Open();
                ArrayList rowList = new ArrayList();
                OracleCommand cmd = new OracleCommand(enrollmentQuery, conn);
                OracleDataReader dr = cmd.ExecuteReader();
                string isDBNull = null;

                    while (dr.Read())

                    {
                        EnrollmentRecordPOCO poco = new EnrollmentRecordPOCO();
                        object[] values = new object[dr.FieldCount];
                        poco.Session_Id = (Decimal)dr["Session_Id"];

                    if (poco.Ucid != isDBNull)
                    {
                        poco.Ucid = (string)dr["ucid"];
                    }
                    else
                    {

                        poco.Ucid = (string)dr["ucid"].ToString();
                    }
                    poco.Timestamp = (string)dr["timestamp"];
                        poco.Activity = (string)dr["activity"];
                        poco.FinalStatus = (string)dr["final_status"];
                        enrollList.Add(poco);
                    }

                


            }
            catch (OracleException ex)
            {
                Console.WriteLine("DataBase Error Had Occured Please check Log File For Details");
                AppLogger.MakeEntry(ex.Message);
                
            }
            finally
            {
                if (oracleDataReader != null)
                {
                    oracleDataReader.Close();
                }
                conn.Close();
            }
            return enrollList;
        }
        #endregion


        #region  GetVerfication records
        public List<VerificationRecordsPOCO> GetVerficationReport()
        {
            OracleDataReader oracleDataReader = null;
            string connection = AppUtils.getConnectionString();

            if (customStartDate != "" && customEndDate != "")
            {
                verificationQuery = verificationQuery.Replace("?START_DATE?", "'" + customStartDate + "'");
                verificationQuery = verificationQuery.Replace("?END_DATE?", "'" + customEndDate + "'");
            }
            else
            {
                var currentDate = DateTime.Now;
                var yesterdayDate = DateTime.Today.AddDays(-1);
                verificationQuery = verificationQuery.Replace("?START_DATE?", "'" + AppUtils.GetDateZeroTime(currentDate).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
                verificationQuery = verificationQuery.Replace("?END_DATE?", "'" + AppUtils.GetDateZeroTime(yesterdayDate).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");

            }
            
            Console.WriteLine(verificationQuery);
            AppLogger.MakeEntry(verificationQuery);
            OracleConnection conn = new OracleConnection(connection);
            List<VerificationRecordsPOCO> verifyList = new List<VerificationRecordsPOCO>();
            try
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand(verificationQuery, conn);
                OracleDataReader dr = cmd.ExecuteReader();
                string isDBNull = null;

                while (dr.Read())
                    {
                        VerificationRecordsPOCO poco = new VerificationRecordsPOCO();
                        object[] values = new object[dr.FieldCount];

                        poco.Session_Id = (Decimal)dr["session_id"];
                    
                    if (poco.Ucid != isDBNull)
                    {
                        poco.Ucid = (string)dr["ucid"];
                    }
                        else
                    {

                        poco.Ucid = (string)dr["ucid"].ToString();
                    }
                        poco.Timestamp = (string)dr["timestamp"];
                        poco.Activity = (string)dr["activity"];
                        poco.FinalStatus = (string)dr["final_status"];
                        verifyList.Add(poco);
                    }
                
            }
            catch (OracleException ex)
            {
                Console.WriteLine("DataBase Error Had Occured Please check Log File For Details");
                AppLogger.MakeEntry(ex.Message);
                
            }
            finally
            {
                if(oracleDataReader!=null)
                {
                    oracleDataReader.Close();
                }
                conn.Close();
            }
            return verifyList;
        }
        #endregion     
    }
}
