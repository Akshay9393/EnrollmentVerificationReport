﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentVerificationReport
{
    class VerificationRecordsPOCO
    {
        public Decimal Session_Id { get; set; }

        public string Ucid { get; set; }

        public string Timestamp { get; set; }

        public string Activity { get; set; }

        public string FinalStatus { get; set; }

        public override string ToString()
        {
            return Session_Id + "     " + Ucid + "    " + Timestamp + "   " + Activity + "    " + FinalStatus;
        }
    }

}
