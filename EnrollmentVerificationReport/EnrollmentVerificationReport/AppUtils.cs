﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace EnrollmentVerificationReport
{
    class AppUtils
    {
        static string customStartDate = System.Configuration.ConfigurationSettings.AppSettings["CustomStartDate"];
        static string customEndDate = System.Configuration.ConfigurationSettings.AppSettings["CustomEndDate"];
        static string connection = System.Configuration.ConfigurationSettings.AppSettings["connection"];
        

        public static string getConnectionString()
        {
            string dbPassword = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"];
            string cn=connection;

            cn += "Password="+Base64Decode(dbPassword)+";";   
            return cn;
        }

        #region DecodeTheEncodedPassword
        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        #endregion
        
        #region get the connection details
        public static string getHaResourceConnectionDetails()
        {
            string conn = AppUtils.getConnectionString();
            //Console.WriteLine(conn);
            OracleConnection con = new OracleConnection(conn);
            string details = string.Empty;
            try
            {
                con.Open();
                OracleCommand cmd = new OracleCommand("select Details from nvb_ha_resources where INTERNAL_NAME='DATABASE1'", con);
                OracleDataReader dr = cmd.ExecuteReader();

                
                    while(dr.Read())
                    {

                        if (!dr.IsDBNull(0))
                        {
                            details = dr.GetString(0);
                        }
                    }
                

                   
            }
            catch (Exception ex)
            {
                AppLogger.MakeEntry(ex.Message);
               
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return details;
        }
        #endregion

        #region Decode the Password
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        #endregion

        #region get DateZero time 
        public static DateTime GetDateZeroTime(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }
        #endregion

        #region get yesterdaytime 
        public static string GetYesterdayDateTime(string prefix)
        {
            var yesterdayDate = DateTime.Today.AddDays(-1);
            if (customStartDate != "" && customEndDate != "")
            {
                yesterdayDate = DateTime.Parse(customStartDate);   
            }
            return prefix+"-"+GetDateZeroTime(yesterdayDate).ToString("yyyymmdd");
        }
        #endregion
    }

  }
