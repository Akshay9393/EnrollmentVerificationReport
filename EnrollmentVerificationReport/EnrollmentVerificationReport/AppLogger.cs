﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnrollmentVerificationReport
{
   static class AppLogger
    {
        static StreamWriter objFile = null;

        private static bool CreateLogFile()
        {
            try
            {

                if (!Directory.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs"))
                {

                    Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs");
                }

                if (!File.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "EnrollmentReport" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt"))
                {
                    File.Create(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "EnrollmentReport" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt").Close();
                }

                objFile = new StreamWriter(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "EnrollmentReport" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt", true);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
                return false;
            }
        }
        public static void MakeEntry(string strLog)
        {
            try
            {
                CreateLogFile();
                if (objFile != null)
                {
                    objFile.WriteLine(strLog);
                    objFile.Close();
                    objFile.Dispose();
                    objFile = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void MakeEntry(string strLog,string fileName)
        {
            try
            {
                CreateLogFile(fileName);
                if (objFile != null)
                {
                    objFile.WriteLine(strLog);
                    objFile.Close();
                    objFile.Dispose();
                    objFile = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
            }
            finally
            {
                GC.Collect();
            }
        }

        private static bool CreateLogFile(string fileName)
        {
            try
            {

                if (!Directory.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Reports"))
                {

                    Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Reports");
                }

                if (!File.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName))
                {
                    File.Create(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName).Close();
                }

                objFile = new StreamWriter(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName, true);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
                return false;
            }
        }
    }
}
